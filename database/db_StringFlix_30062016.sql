-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 30, 2016 at 08:28 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_StringFlix`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_access_tokens`
--

CREATE TABLE IF NOT EXISTS `auth_access_tokens` (
  `access_token_id` int(10) NOT NULL,
  `access_token` varchar(40) NOT NULL,
  `user_id` int(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `is_sync` enum('0','1') NOT NULL DEFAULT '0',
  `platform` varchar(10) NOT NULL DEFAULT 'ios',
  `device_token` varchar(100) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `build` enum('test','live') NOT NULL DEFAULT 'live'
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_access_tokens`
--

INSERT INTO `auth_access_tokens` (`access_token_id`, `access_token`, `user_id`, `active`, `is_sync`, `platform`, `device_token`, `created`, `build`) VALUES
(1, 'bbfadb774117f708cf98a284250afa380551041e', 1, 1, '0', 'ios', '0', '2016-04-25 11:51:54', 'live'),
(2, '32b04851686b9e01cea541345d29d52e5bf1e0fd', 1, 1, '0', 'ios', '0', '2016-04-25 12:16:23', 'live'),
(3, '2efbd6737f90c891c530fea611abbe58ff9cc5f4', 1, 1, '0', 'ios', '0', '2016-04-25 12:18:02', 'live'),
(4, 'd6bea2918f603fdb9fcd49d0e2ba13ddee211182', 1, 1, '0', 'ios', '0', '2016-04-25 12:19:04', 'live'),
(5, 'f9ef1d10952d246fccb288742b726bd3b92b8f5a', 1, 1, '0', 'ios', '0', '2016-04-25 12:52:18', 'live'),
(6, '5f648815c6c6d09d65219312f87ea948e4e23c2d', 1, 1, '0', 'ios', '0', '2016-04-26 05:02:10', 'live'),
(7, '530c25a5181c025125ed9d99c3a262cabfb53406', 2, 1, '0', 'ios', '0', '2016-04-26 06:08:52', 'live'),
(8, 'cba373cdec6c739efaf9fee24c62ee50d33d7746', 2, 1, '0', 'ios', '0', '2016-04-26 06:15:58', 'live'),
(9, '260c8e56536f78a3bada90acc6da4df773253170', 2, 1, '0', 'ios', '0', '2016-04-26 06:18:13', 'live'),
(10, '4b12fa6a76d98074788e00f09a10a73e7ae7bc96', 2, 1, '0', 'ios', '0', '2016-04-26 06:55:26', 'live'),
(11, '45559f6c0b3d06e2e3d7edc36fe65a23ad2d9f69', 7, 1, '0', 'ios', '0', '2016-04-26 10:11:09', 'live'),
(12, '3bfe9fc8611eae86ec92dd8f8e0269aa9484c3a9', 7, 1, '0', 'ios', '0', '2016-04-26 10:12:36', 'live'),
(13, 'a30322251f8efae9564d1949c606005270e23893', 7, 1, '0', 'ios', '0', '2016-05-04 07:40:45', 'live'),
(14, '044330cdabf1fa80033d5d6e50720f0599241a09', 22, 1, '0', 'ios', '0', '2016-05-13 06:22:01', 'live'),
(15, '93da7b78696ba1d3c4b091b73d468a3e2ee8e833', 19, 1, '0', 'ios', '0', '2016-05-13 06:34:03', 'live'),
(16, '56b5bfc80ab05901ef2e88379ffe49169ebfd695', 19, 1, '0', 'ios', '0', '2016-05-13 10:48:44', 'live'),
(17, 'd211882751818adfabdcaa3119e51f4c9d2f0e8a', 24, 1, '0', 'ios', '0', '2016-05-13 11:14:53', 'test'),
(18, 'b2c27a9696a1228e078e440f028ebb0dfc15c55d', 24, 1, '0', 'ios', '0', '2016-05-13 11:15:41', 'test'),
(19, '93ceb73c43ed4e561635ed2598cd4b5e0e759041', 25, 1, '0', 'ios', '0', '2016-05-13 11:17:35', 'test'),
(20, 'ef222727af414e6b30730ed5dfa2a403dd2a3a4a', 19, 1, '0', 'ios', '0', '2016-05-13 11:21:14', 'live'),
(21, 'd92563bcd2d275bd57c4f43eed8f07f001e421b0', 26, 1, '0', 'ios', '0', '2016-05-13 11:23:49', 'live'),
(22, 'e094ac9f5160c0a2854f2fe2b11c00921a44a127', 26, 1, '0', 'ios', '0', '2016-05-13 11:24:38', 'test'),
(23, '93b30597fa3dc829e3fa94f246fe8b22e82c9f6e', 26, 1, '0', 'ios', '0', '2016-05-13 11:25:42', 'live'),
(24, 'e20b785335c5b524dd407d714d095df92eddb0d3', 26, 1, '0', 'ios', '0', '2016-05-13 11:25:47', 'live'),
(25, '1af4a8d49c389117f99ea5e4a0619da7418da1b0', 26, 1, '0', 'ios', '0', '2016-05-13 11:25:48', 'live'),
(26, '94456ec80de691cf62de1f0ea325c80234523423', 26, 1, '0', 'ios', '0', '2016-05-13 11:25:54', 'live'),
(27, '04b3d9fd7629c3fa1eedfe6341472f126b152947', 26, 1, '0', 'ios', '0', '2016-05-13 11:26:03', 'test'),
(28, '82416c1185b20d84e8dfe28aa2cf0e450a3a4bba', 26, 1, '0', 'ios', '0', '2016-05-13 11:28:20', 'test'),
(29, '22016c0aaa0d5976c73e42a656a4b6c1a93f493f', 19, 1, '0', 'ios', '0', '2016-05-13 11:32:38', 'live'),
(30, 'bff1eec1513b880d54ab54263db24f7bfd12a072', 26, 1, '0', 'ios', '0', '2016-05-13 11:46:21', 'live'),
(31, '732d4a18a950c5845f64280dd60ec4e02b9a66ab', 26, 1, '0', 'ios', '0', '2016-05-13 11:47:42', 'live'),
(32, 'ad816c6167c9bae0b265d71a9273e6d96709f9b9', 26, 1, '0', 'ios', '0', '2016-05-13 11:48:06', 'live'),
(33, '741e391ec646c12048a90a98308922494462a5ff', 26, 1, '0', 'ios', '0', '2016-05-13 11:49:04', 'live'),
(34, 'ee1357e51e2abec4143fa3e9aa673aece6120dd9', 26, 1, '0', 'ios', '0', '2016-05-13 11:51:17', 'live'),
(35, '9eec20c12d23b226f8d35e735e4174f2db648b00', 26, 1, '0', 'ios', '0', '2016-05-13 11:56:43', 'live'),
(36, '7931a0d0684fdf423c63443540b4e08ba152646f', 26, 1, '0', 'ios', '0', '2016-05-13 11:57:11', 'live'),
(37, 'f6cd73ab48144e91d1ec2c417dbeed6bd70f1d81', 26, 1, '0', 'ios', '0', '2016-05-13 11:59:56', 'live'),
(38, 'bae4a8c592c35cae61861be01713f4047701098b', 26, 1, '0', 'ios', '0', '2016-05-13 12:05:26', 'live'),
(39, 'fd3992dd0570a27245694039bc4b7f0e7bdc90af', 26, 1, '0', 'ios', '0', '2016-05-13 12:06:46', 'live'),
(40, '2159469ae3aced7ba65ff3dcb9520febae143a2c', 26, 1, '0', 'ios', '0', '2016-05-13 12:29:09', 'live'),
(41, '29cf6125c6a22021a7d94f4be4a9265d90c3aa84', 26, 1, '0', 'ios', '0', '2016-05-16 09:51:03', 'live'),
(42, 'e3909cd241ea5df6514c36204a78dee7b72b6379', 26, 1, '0', 'ios', '0', '2016-05-17 05:49:40', 'live'),
(43, '45439da49e6512376705eecbd1fde23241b87c5c', 26, 1, '0', 'ios', '0', '2016-05-17 05:50:49', 'live'),
(44, '6a8d8e74caea3944ab1d3fde65e0782b66b003e4', 26, 1, '0', 'ios', '0', '2016-05-17 06:02:36', 'live'),
(45, 'ca3acec5d0bddfd75a4eca6d04f9a13c63b2b507', 26, 1, '0', 'ios', '0', '2016-05-17 06:23:27', 'live'),
(46, '4ae1274875676bcfad442ce4d2c6f41268facc28', 26, 1, '0', 'ios', '0', '2016-05-17 06:53:44', 'live'),
(47, '4e797068a0e02e87291240df46899fd7cdc533db', 26, 1, '0', 'ios', '0', '2016-05-17 07:17:28', 'live'),
(48, '0dc947f6d4129b2f8e92c6fbfa03e0117d64c508', 26, 1, '0', 'ios', '0', '2016-05-17 07:19:03', 'live'),
(49, '5208874ea485f736fe7b2aa2bd216422975cdfb5', 26, 1, '0', 'ios', '0', '2016-05-17 07:47:35', 'live'),
(50, '03a3352e79ed68e0f2b4fcee4215d78152051a4b', 26, 1, '0', 'ios', '0', '2016-05-18 05:45:18', 'live'),
(51, 'f56c0cdd76e944867115f0af9c70b869726881a6', 26, 1, '0', 'ios', '0', '2016-05-18 05:56:50', 'live'),
(52, '0348636263771b8d5c58a529a43a952d24491581', 26, 1, '0', 'ios', '0', '2016-05-18 06:01:52', 'live'),
(53, '8f1255b28a3b3712a8c4e1943610ccc335c81af0', 26, 1, '0', 'ios', '0', '2016-05-18 07:51:20', 'live'),
(54, 'de812c70aeb1cfa942c2828fb929c352276b05dc', 7, 1, '0', 'ios', '0', '2016-05-18 08:27:50', 'live'),
(55, 'e61aa9d76435a7b2e60d287ac93b3c2bcc9a6320', 26, 1, '0', 'ios', '0', '2016-05-18 10:13:05', 'live'),
(56, '17bd6513f59a1cdf0bf37a6ee16443e52a16a7fd', 26, 1, '0', 'ios', '0', '2016-05-18 11:33:20', 'live'),
(57, '27692332d02eccd0358c4b929db6ff8e9cacc62f', 26, 1, '0', 'ios', '0', '2016-05-18 11:34:16', 'live'),
(58, '33c1244fde1bfdf31e061c37fd2671419fe2700e', 26, 1, '0', 'ios', '0', '2016-05-18 11:35:04', 'live'),
(59, 'ca44009e0ed3120288ddb159899b44ac7b855110', 26, 1, '0', 'ios', '0', '2016-05-18 11:36:33', 'live'),
(60, 'af4fee12c04baaef75c385829e27506c79c9f820', 26, 1, '0', 'ios', '0', '2016-05-18 11:37:23', 'live'),
(61, 'b4cea2db9c20ed722438a6a8f0e86767d8591cea', 26, 1, '0', 'ios', '0', '2016-05-18 11:58:18', 'live'),
(62, '65b2b6811249499443b4a5995bbfef044721f0c1', 26, 1, '0', 'ios', '0', '2016-05-18 12:15:26', 'live'),
(63, '541c8a9bc7e969f06ae18ad954f05c023b034e60', 26, 1, '0', 'ios', '0', '2016-05-19 11:33:31', 'live'),
(64, '498c3342a76185cc98520ccc53f57ba8c6e80bdb', 26, 1, '0', 'ios', '0', '2016-05-19 11:46:23', 'live'),
(65, '823ff4554b2de3188af0c7e71fb58b352d7a923f', 26, 1, '0', 'ios', '0', '2016-05-20 05:53:43', 'live'),
(66, 'f45f615f4e504c004c37e8400c9036511325936e', 26, 1, '0', 'ios', '0', '2016-05-20 10:29:49', 'live'),
(67, 'e985566d6c03077bce146b5a9f2fa0c8873b1c7e', 26, 1, '0', 'ios', '0', '2016-05-20 10:35:34', 'live'),
(68, 'c38fb2a6088be28a9ea584cc0713a918cedbd58c', 26, 1, '0', 'ios', '0', '2016-05-20 10:37:23', 'live'),
(69, '81c20c86259ad7d7752f0ad3de8a8f2df404e754', 26, 1, '0', 'ios', '0', '2016-05-20 10:45:58', 'live'),
(70, '8430131d074e9dcd2a862fdbe338a4e721c2508a', 26, 1, '0', 'ios', '0', '2016-05-20 10:47:24', 'live'),
(71, 'fb7082a81cb1c603cc3b07012b4438282ffbc21c', 26, 1, '0', 'ios', '0', '2016-05-20 10:48:56', 'live'),
(72, '90aa3dc8606198c969cd324873513a501e91b862', 26, 1, '0', 'ios', '0', '2016-05-20 10:49:56', 'live'),
(73, 'c941b3766f08d74bcb52f15374eb669bf576ff51', 26, 1, '0', 'ios', '0', '2016-05-20 10:59:36', 'live'),
(74, '4e9137fb71ec531b49a83b700bb30dfd558da320', 26, 1, '0', 'ios', '0', '2016-05-20 11:02:15', 'live'),
(75, '24a9774a87aaf25bd5adca45e2847e6a2c90f1c1', 26, 1, '0', 'ios', '0', '2016-05-20 11:03:55', 'live'),
(76, '2da95d2c8d4fa6de891074c21a973fdc2321a4e9', 26, 1, '0', 'ios', '0', '2016-05-20 11:05:07', 'live'),
(77, '01e9eaefa40b75547d9ac97b4e99da88b3a47cc0', 26, 1, '0', 'ios', '0', '2016-05-20 11:09:24', 'live'),
(78, '6e0721e657802466807c405c3bc9ef6e8a887b55', 26, 1, '0', 'ios', '0', '2016-05-20 11:10:19', 'live'),
(79, '3060ba73c2dfead7c288dab08a813ec4f49110c3', 26, 1, '0', 'ios', '0', '2016-05-20 11:11:50', 'live'),
(80, 'dcd20902b1a637c6e5557a2304ecb270bff1d83b', 26, 1, '0', 'ios', '0', '2016-05-20 11:14:00', 'live'),
(81, 'd9e779402e49893c9852b7f73419608bdc9bd0d3', 26, 1, '0', 'ios', '0', '2016-05-20 11:15:08', 'live'),
(82, '63a62985d4967870b33819c7a5242ad6fdd7d2f7', 26, 1, '0', 'ios', '0', '2016-05-20 11:16:43', 'live'),
(83, '139cf0b819762962719e0e770bf25809085e0567', 26, 1, '0', 'ios', '0', '2016-05-20 11:19:45', 'live'),
(84, 'fa959a82c795b4957dc80f3d1b36a64f7e081636', 26, 1, '0', 'ios', '0', '2016-05-20 11:20:30', 'live'),
(85, '4ccb9bff84fdeb04d4d5e0fa18931e42ab7b8d72', 26, 1, '0', 'ios', '0', '2016-05-20 11:23:00', 'live'),
(86, '2f85999e73ce6fdb996cbfea0b9d83a36253b085', 26, 1, '0', 'ios', '0', '2016-05-20 11:26:25', 'live'),
(87, '8b029a10b3e2f43e1f3aa3458e11f590306d6cc3', 26, 1, '0', 'ios', '0', '2016-05-20 11:27:21', 'live'),
(88, '5d0d04c49325cbc72309b67d1a205c773237bee9', 26, 1, '0', 'ios', '0', '2016-05-20 11:36:19', 'live'),
(89, '80ad26075bd800d217b749fb7408384232c97a77', 26, 1, '0', 'ios', '0', '2016-05-20 12:36:26', 'live'),
(90, 'ac4a854cda8bce3d8f4db8b5f47b2edfca652808', 26, 1, '0', 'ios', '0', '2016-05-20 12:39:51', 'live'),
(91, 'bdf053b86f6dc0e0b03355bf0c13effde5a03d95', 26, 1, '0', 'ios', '0', '2016-05-20 12:40:35', 'live'),
(92, 'af4bc3f0bb3622fb5c491614f39dc3e2e813b6fd', 26, 1, '0', 'ios', '0', '2016-05-20 12:42:14', 'live');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `msg_id` int(20) NOT NULL,
  `msg` text NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`msg_id`, `msg`, `created`) VALUES
(1, 'M1-T', '2016-05-17 11:03:54'),
(2, 'M1-T2', '2016-05-17 11:04:17'),
(3, 'fsdf', '2016-05-17 11:05:15'),
(4, 'fdfsd', '2016-05-17 11:05:18'),
(5, 'dfdsf', '2016-05-17 11:05:20'),
(6, 'ddasd', '2016-05-17 12:01:27'),
(7, 'dsd', '2016-05-17 12:01:33'),
(8, 'aaa', '2016-05-17 12:02:10'),
(9, 'hiiiii', '2016-05-17 13:14:44'),
(10, 'hiiiii', '2016-05-17 13:15:20'),
(11, 'hiiiii', '2016-05-17 13:17:42'),
(12, 'hiiiii', '2016-05-17 13:17:58'),
(13, 'hiiiii', '2016-05-17 13:20:15'),
(14, 'hiiiii', '2016-05-17 13:21:43'),
(15, 'hiiiii', '2016-05-17 13:27:23'),
(16, 'hiiiii', '2016-05-17 13:33:33'),
(17, 'gfgfsgsfdgs', '2016-05-18 13:55:11'),
(18, 'dfdssdfdsfds', '2016-05-18 13:58:13'),
(19, 'hello shubham', '2016-05-18 13:58:38'),
(20, 'hey', '2016-05-18 13:58:50'),
(21, 'ererwer', '2016-05-19 17:34:36'),
(22, 'fgd', '2016-05-19 17:34:42'),
(23, 'xccvxcxc', '2016-05-20 18:12:43'),
(24, 'ghfgfhgfh', '2016-05-20 18:13:00'),
(25, 'fdgdfgdf', '2016-05-20 18:13:34'),
(26, 'fgghfgffghfhgf', '2016-05-20 18:14:06'),
(27, 'ghfghghfgh', '2016-05-20 18:14:33'),
(28, 'fggggggggggggggggggggggg', '2016-05-20 18:33:17');

-- --------------------------------------------------------

--
-- Table structure for table `message_recipients`
--

CREATE TABLE IF NOT EXISTS `message_recipients` (
  `id` int(20) NOT NULL,
  `msg_id` int(20) NOT NULL,
  `from` int(11) NOT NULL,
  `to` int(20) NOT NULL,
  `type` enum('group','indivisual') NOT NULL DEFAULT 'group',
  `group_id` int(11) NOT NULL,
  `is_read` enum('0','1') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message_recipients`
--

INSERT INTO `message_recipients` (`id`, `msg_id`, `from`, `to`, `type`, `group_id`, `is_read`) VALUES
(1, 1, 0, 20, 'group', 0, '0'),
(2, 1, 0, 44, 'group', 0, '0'),
(3, 2, 0, 20, 'group', 0, '0'),
(4, 2, 0, 44, 'group', 0, '0'),
(5, 3, 0, 20, 'group', 0, '0'),
(6, 3, 0, 44, 'group', 0, '0'),
(7, 4, 0, 20, 'group', 0, '0'),
(8, 4, 26, 26, 'group', 9, '0'),
(9, 5, 26, 7, 'group', 9, '0'),
(10, 5, 26, 27, 'group', 9, '0'),
(11, 16, 7, 26, 'group', 9, '0'),
(12, 16, 7, 7, 'group', 9, '0'),
(13, 16, 7, 27, 'group', 9, '0'),
(14, 17, 26, 26, 'group', 9, '0'),
(15, 17, 26, 7, 'group', 9, '0'),
(16, 17, 26, 27, 'group', 9, '0'),
(17, 18, 7, 26, 'group', 9, '0'),
(18, 18, 7, 7, 'group', 9, '0'),
(19, 18, 7, 27, 'group', 9, '0'),
(20, 19, 26, 26, 'group', 9, '0'),
(21, 19, 26, 7, 'group', 9, '0'),
(22, 19, 26, 27, 'group', 9, '0'),
(23, 20, 7, 26, 'group', 9, '0'),
(24, 20, 7, 7, 'group', 9, '1'),
(25, 20, 7, 27, 'group', 9, '0'),
(26, 21, 26, 26, 'group', 10, '0'),
(27, 21, 26, 7, 'group', 10, '0'),
(28, 21, 26, 23, 'group', 10, '0'),
(29, 21, 26, 23, 'group', 10, '0'),
(30, 21, 26, 29, 'group', 10, '0'),
(31, 21, 26, 29, 'group', 10, '0'),
(32, 21, 26, 29, 'group', 10, '0'),
(33, 22, 26, 26, 'group', 10, '0'),
(34, 22, 26, 7, 'group', 10, '0'),
(35, 22, 26, 23, 'group', 10, '0'),
(36, 22, 26, 23, 'group', 10, '0'),
(37, 22, 26, 29, 'group', 10, '0'),
(38, 22, 26, 29, 'group', 10, '0'),
(39, 22, 26, 29, 'group', 10, '0'),
(40, 23, 26, 26, 'group', 15, '0'),
(41, 24, 26, 26, 'group', 15, '0'),
(42, 25, 26, 26, 'group', 15, '0'),
(43, 26, 26, 26, 'group', 13, '0'),
(44, 27, 26, 26, 'group', 13, '0'),
(45, 28, 26, 26, 'group', 13, '0');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `notification_id` int(20) NOT NULL,
  `come_from` int(20) NOT NULL,
  `send_to` int(20) NOT NULL,
  `notification` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` varchar(250) NOT NULL,
  `is_read` enum('0','1') NOT NULL DEFAULT '0' COMMENT '1 for read,0 for pending'
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`notification_id`, `come_from`, `send_to`, `notification`, `date`, `type`, `is_read`) VALUES
(15, 7, 15, 'you are invited', '2016-05-11 11:35:51', '', '1'),
(16, 7, 16, 'you are invited', '2016-05-11 11:35:51', '', '0'),
(17, 7, 15, 'you are invited', '2016-05-11 11:37:09', '', '0'),
(18, 7, 16, 'you are invited', '2016-05-11 11:37:09', '', '0'),
(23, 7, 17, 'testproject is deleted', '2016-05-11 12:40:02', '', '1'),
(26, 7, 14, 'testproject is deleted', '2016-05-11 12:40:02', '', '0'),
(27, 7, 14, 'testproject is deleted', '2016-05-11 12:40:02', '', '0'),
(28, 7, 14, 'testproject is deleted', '2016-05-11 12:40:02', '', '0'),
(29, 7, 15, 'testproject is deleted', '2016-05-11 12:40:02', '', '0'),
(30, 7, 7, 'testproject is deleted', '2016-05-11 12:40:02', '', '0'),
(31, 7, 8, 'test project is deleted', '2016-05-11 12:45:38', '', '0'),
(32, 7, 7, 'test project is deleted', '2016-05-11 12:45:38', '', '0'),
(33, 7, 8, 'test project is deleted', '2016-05-11 12:45:38', '', '0'),
(34, 7, 7, 'test project is deleted', '2016-05-11 12:45:38', '', '0'),
(35, 7, 13, 'test project is deleted', '2016-05-11 12:45:38', '', '0'),
(36, 7, 8, 'test project is deleted', '2016-05-11 12:45:38', '', '0'),
(37, 7, 7, 'test project is deleted', '2016-05-11 12:45:38', '', '0'),
(38, 7, 14, 'test project is deleted', '2016-05-11 12:45:38', '', '0'),
(39, 7, 14, 'test project is deleted', '2016-05-11 12:45:38', '', '0'),
(40, 7, 14, 'test project is deleted', '2016-05-11 12:45:38', '', '0'),
(41, 7, 7, 'test project is deleted', '2016-05-11 12:45:38', '', '0'),
(42, 7, 15, 'test project is deleted', '2016-05-11 12:45:38', '', '0'),
(43, 7, 15, 'test  project is deleted', '2016-05-11 12:47:10', '', '0'),
(44, 7, 15, 'test  project is deleted', '2016-05-11 12:47:10', '', '0'),
(45, 7, 7, 'test  project is deleted', '2016-05-11 12:47:10', '', '0'),
(46, 7, 15, 'test  project is deleted', '2016-05-11 12:47:10', '', '0'),
(47, 7, 16, 'test  project is deleted', '2016-05-11 12:47:10', '', '0'),
(48, 7, 15, 'test  project is deleted', '2016-05-11 12:47:59', '', '0'),
(49, 7, 15, 'test  project is deleted', '2016-05-11 12:47:59', '', '0'),
(50, 7, 16, 'test  project is deleted', '2016-05-11 12:47:59', '', '0'),
(51, 7, 15, 'test  project is deleted', '2016-05-11 12:47:59', '', '0'),
(52, 7, 16, 'test  project is deleted', '2016-05-11 12:47:59', '', '0'),
(53, 7, 15, 'test  project is deleted', '2016-05-11 12:48:54', '', '0'),
(54, 7, 15, 'test  project is deleted', '2016-05-11 12:48:54', '', '0'),
(55, 7, 16, 'test  project is deleted', '2016-05-11 12:48:54', '', '0'),
(56, 7, 15, 'test  project is deleted', '2016-05-11 12:48:54', '', '0'),
(57, 7, 16, 'test  project is deleted', '2016-05-11 12:48:54', '', '0'),
(58, 7, 17, 'you are invited', '2016-05-11 12:53:48', '', '0'),
(59, 7, 18, 'you are invited', '2016-05-11 12:53:48', '', '0'),
(60, 7, 17, 'you are invited', '2016-05-11 12:56:34', '', '0'),
(61, 7, 18, 'you are invited', '2016-05-11 12:56:34', '', '0'),
(62, 7, 8, 'test  project is deleted', '2016-05-11 13:00:28', '', '0'),
(63, 7, 8, 'test  project is deleted', '2016-05-11 13:00:28', '', '0'),
(64, 7, 8, 'test  project is deleted', '2016-05-11 13:00:28', '', '0'),
(65, 7, 12, 'test  project is deleted', '2016-05-11 13:00:28', '', '0'),
(66, 7, 13, 'test  project is deleted', '2016-05-11 13:00:28', '', '0'),
(67, 7, 8, 'test  project is deleted', '2016-05-11 13:00:28', '', '0'),
(68, 7, 8, 'test  project is deleted', '2016-05-11 13:00:28', '', '0'),
(69, 7, 14, 'test  project is deleted', '2016-05-11 13:00:28', '', '0'),
(70, 7, 14, 'test  project is deleted', '2016-05-11 13:00:28', '', '0'),
(71, 7, 14, 'test  project is deleted', '2016-05-11 13:00:28', '', '0'),
(72, 7, 15, 'test  project is deleted', '2016-05-11 13:00:28', '', '0'),
(73, 7, 15, 'test  project is deleted', '2016-05-11 13:00:28', '', '0'),
(74, 7, 8, 'test  project is deleted', '2016-05-11 13:02:54', '', '0'),
(75, 7, 8, 'test  project is deleted', '2016-05-11 13:02:54', '', '0'),
(76, 7, 8, 'test  project is deleted', '2016-05-11 13:02:54', '', '0'),
(77, 7, 12, 'test  project is deleted', '2016-05-11 13:02:54', '', '0'),
(78, 7, 13, 'test  project is deleted', '2016-05-11 13:02:54', '', '0'),
(79, 7, 8, 'test  project is deleted', '2016-05-11 13:02:54', '', '0'),
(80, 7, 8, 'test  project is deleted', '2016-05-11 13:02:54', '', '0'),
(81, 7, 14, 'test  project is deleted', '2016-05-11 13:02:54', '', '0'),
(82, 7, 14, 'test  project is deleted', '2016-05-11 13:02:54', '', '0'),
(83, 7, 14, 'test  project is deleted', '2016-05-11 13:02:54', '', '0'),
(84, 7, 15, 'test  project is deleted', '2016-05-11 13:02:54', '', '0'),
(85, 7, 15, 'test  project is deleted', '2016-05-11 13:02:54', '', '0'),
(86, 7, 15, 'test  project is deleted', '2016-05-11 13:03:18', '', '0'),
(87, 7, 15, 'test  project is deleted', '2016-05-11 13:03:18', '', '0'),
(88, 7, 16, 'test  project is deleted', '2016-05-11 13:03:18', '', '0'),
(89, 7, 15, 'test  project is deleted', '2016-05-11 13:03:18', '', '0'),
(90, 7, 16, 'test  project is deleted', '2016-05-11 13:03:18', '', '0'),
(91, 7, 17, 'test  project is deleted', '2016-05-11 13:03:18', '', '0'),
(92, 7, 18, 'test  project is deleted', '2016-05-11 13:03:18', '', '0'),
(93, 7, 17, 'test  project is deleted', '2016-05-11 13:03:18', '', '0'),
(94, 7, 18, 'test  project is deleted', '2016-05-11 13:03:18', '', '0'),
(95, 7, 19, 'you are invited', '2016-05-11 17:23:01', '', '0'),
(96, 7, 20, 'you are invited', '2016-05-11 17:23:01', '', '0'),
(97, 7, 21, 'you are invited', '2016-05-11 17:26:29', '', '0'),
(98, 7, 15, 'test  project is deleted', '2016-05-11 17:35:50', '', '0'),
(99, 7, 15, 'test  project is deleted', '2016-05-11 17:35:50', '', '0'),
(100, 7, 16, 'test  project is deleted', '2016-05-11 17:35:50', '', '0'),
(101, 7, 15, 'test  project is deleted', '2016-05-11 17:35:50', '', '0'),
(102, 7, 16, 'test  project is deleted', '2016-05-11 17:35:50', '', '0'),
(103, 7, 17, 'test  project is deleted', '2016-05-11 17:35:50', '', '0'),
(104, 7, 18, 'test  project is deleted', '2016-05-11 17:35:50', '', '0'),
(105, 7, 17, 'test  project is deleted', '2016-05-11 17:35:50', '', '0'),
(106, 7, 18, 'test  project is deleted', '2016-05-11 17:35:50', '', '0'),
(107, 7, 19, 'test  project is deleted', '2016-05-11 17:35:50', '', '0'),
(108, 7, 20, 'test  project is deleted', '2016-05-11 17:35:50', '', '0'),
(109, 7, 21, 'test  project is deleted', '2016-05-11 17:35:50', '', '0'),
(110, 7, 23, 'you are invited', '2016-05-13 12:43:39', '', '0'),
(111, 7, 23, 'testtest  project is deleted', '2016-05-13 12:53:09', '', '0'),
(112, 7, 23, 'testtest  project is deleted', '2016-05-13 12:53:09', '', '0'),
(113, 7, 23, 'testtest  project is deleted', '2016-05-13 12:54:33', '', '0'),
(114, 7, 23, 'testtest  project is deleted', '2016-05-13 12:54:33', '', '1'),
(115, 17, 7, 'reject invitation', '2016-05-13 13:12:31', '', '0'),
(116, 26, 26, 'you are invited', '2016-05-17 13:15:06', '', '0'),
(117, 26, 26, 'you are invited', '2016-05-17 13:18:31', '', '0'),
(118, 26, 26, 'you are invited', '2016-05-17 13:21:37', '', '0'),
(119, 7, 27, 'you are invited', '2016-05-17 13:23:28', '', '0'),
(120, 7, 27, 'you are invited', '2016-05-17 13:26:45', '', '0'),
(121, 7, 27, 'you are invited', '2016-05-17 13:27:02', '', '0'),
(122, 26, 26, 'you are invited', '2016-05-17 13:33:51', '', '0'),
(123, 26, 26, 'you are invited', '2016-05-17 19:03:02', '', '0'),
(124, 26, 28, 'you are invited', '2016-05-17 19:03:02', '', '0'),
(125, 26, 26, 'you are invited', '2016-05-17 19:05:50', '', '0'),
(126, 26, 28, 'you are invited', '2016-05-17 19:05:50', '', '0'),
(127, 7, 29, 'you are invited for  project', '2016-05-18 15:58:25', '', '0'),
(128, 7, 29, 'you are invited for testtest project', '2016-05-18 16:02:32', '', '0'),
(129, 7, 29, 'you are invited for testtest project', '2016-05-18 16:03:15', '', '0'),
(130, 7, 23, 'you are invited for test1 project by shubham shubham', '2016-05-19 19:01:54', '', '0'),
(131, 7, 32, 'you are invited for testtest project by shubham saini', '2016-05-23 11:35:23', '', '0'),
(132, 7, 33, 'you are invited for testtest project by shubham saini', '2016-05-23 11:35:25', '', '0'),
(133, 7, 34, 'you are invited for testtest project by shubham saini', '2016-05-23 11:39:15', '', '0'),
(134, 7, 35, 'you are invited for testtest project by shubham saini', '2016-05-23 11:39:18', '', '0'),
(135, 7, 36, 'Congratulations, shubham saini has invited you for testtest Flix', '2016-05-26 17:35:58', '', '0'),
(136, 33, 7, 'invitation accepted fordfsf by  ', '2016-05-27 19:13:16', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `project_id` int(20) NOT NULL,
  `project_name` varchar(252) NOT NULL,
  `deadline` datetime NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_deleted` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 for delete, 1 for not'
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`project_id`, `project_name`, `deadline`, `created_date`, `is_deleted`) VALUES
(5, 'test', '2015-12-15 12:12:12', '2016-05-10 18:22:02', '1'),
(8, 'test', '2016-07-21 12:12:12', '2016-05-10 18:40:25', '1'),
(9, 'hiiiiiiiiiiii', '2012-12-15 00:00:00', '2016-05-11 16:56:08', '1'),
(10, 'testtest', '2015-12-15 12:12:12', '2016-05-13 12:23:44', '1'),
(11, 'zczxz', '2016-05-11 00:00:00', '2016-05-13 17:35:12', '1'),
(12, 'firstProject', '2016-05-19 00:00:00', '2016-05-17 12:24:58', '1'),
(13, 'project', '2016-05-28 00:00:00', '2016-05-17 12:33:09', '1'),
(14, 'projectTitle', '2016-05-26 00:00:00', '2016-05-17 12:46:09', '1'),
(15, 'projectabc', '2016-05-28 00:00:00', '2016-05-17 12:48:03', '1'),
(16, 'firstProject', '2016-05-27 00:00:00', '2016-05-17 12:48:27', '1'),
(17, 'first', '2016-05-31 00:00:00', '2016-05-17 12:49:39', '1'),
(18, 'hjhsfddjf', '2016-05-26 00:00:00', '2016-05-17 12:51:08', '1'),
(19, 'abc', '2016-08-26 00:00:00', '2016-05-17 12:56:58', '1'),
(20, 'aaa', '2016-05-27 00:00:00', '2016-05-17 12:58:11', '1'),
(21, 'aaa', '2016-05-31 00:00:00', '2016-05-17 13:00:02', '1'),
(22, 'aaa', '2016-05-27 00:00:00', '2016-05-17 13:04:27', '1'),
(23, 'asdsd', '2016-05-26 00:00:00', '2016-05-17 13:08:23', '1'),
(24, 'sdf', '2016-05-25 00:00:00', '2016-05-17 13:09:55', '1'),
(25, 'aadsd', '2016-05-27 00:00:00', '2016-05-17 13:11:19', '1'),
(26, 'dgdgfgd', '2016-05-28 00:00:00', '2016-05-17 13:13:14', '1'),
(27, 'cbvb', '2016-05-27 00:00:00', '2016-05-17 13:14:48', '1'),
(28, 'dfdsf', '2016-05-26 00:00:00', '2016-05-17 13:18:11', '1'),
(29, 'dsfs', '2016-05-25 00:00:00', '2016-05-17 13:21:03', '1'),
(30, 'fsdsd', '2016-05-27 00:00:00', '2016-05-17 13:33:43', '1'),
(31, 'rwee', '2016-05-26 00:00:00', '2016-05-17 13:58:08', '1'),
(32, 'dfs', '2016-05-27 00:00:00', '2016-05-17 15:19:59', '1'),
(33, 'sdadsd', '2016-05-25 00:00:00', '2016-05-17 15:21:16', '1'),
(34, 'gdddgf', '2016-05-25 00:00:00', '2016-05-17 15:23:55', '1'),
(35, 'fsdfdfdsf', '2016-05-26 00:00:00', '2016-05-17 15:28:11', '1'),
(36, 'fsdfdfdsf', '2016-05-26 00:00:00', '2016-05-17 15:28:29', '1'),
(37, 'sdffg', '2016-05-26 00:00:00', '2016-05-17 17:29:49', '1'),
(38, 'dsfdf', '2016-09-28 00:00:00', '2016-05-17 18:14:08', '1'),
(39, 'gdfg', '2016-05-27 00:00:00', '2016-05-17 18:26:20', '1'),
(40, 'vxgc', '2016-05-27 00:00:00', '2016-05-17 18:39:08', '1'),
(41, 'fdgfdg', '2016-05-26 00:00:00', '2016-05-17 18:42:39', '1'),
(42, 'etgfrgf', '2016-05-27 00:00:00', '2016-05-17 18:48:06', '1'),
(43, 'cxvc', '2016-05-27 00:00:00', '2016-05-17 19:02:40', '1'),
(44, 'dfdsfdf', '2016-05-27 00:00:00', '2016-05-17 19:05:13', '1'),
(45, 'dummy', '2016-05-26 00:00:00', '2016-05-18 17:18:46', '1'),
(46, 'dummyproject', '2016-05-31 00:00:00', '2016-05-18 17:28:48', '1'),
(47, 'dproject', '2016-05-25 00:00:00', '2016-05-18 17:46:01', '1'),
(48, 'rerwere', '2016-05-31 00:00:00', '2016-05-19 17:03:47', '1'),
(49, 'dummy', '2016-05-25 00:00:00', '2016-05-19 17:04:16', '1'),
(50, 'gfhgf', '2016-05-31 00:00:00', '2016-05-19 17:08:45', '1'),
(51, 'dfsf', '2016-05-31 00:00:00', '2016-05-19 17:09:34', '1'),
(52, 'dfdsfsd', '2016-05-31 00:00:00', '2016-05-19 17:25:40', '1'),
(53, 'dsfgsd', '2016-05-31 00:00:00', '2016-05-19 17:25:59', '1'),
(54, 'dfsfsfas', '2016-05-31 00:00:00', '2016-05-19 17:35:10', '1'),
(55, 'Dummy', '2016-05-31 00:00:00', '2016-05-19 17:41:40', '1'),
(56, 'ppppp', '2016-05-31 08:24:00', '2016-05-20 11:55:10', '1'),
(57, 'dgfdg', '2016-05-31 08:31:00', '2016-05-20 12:02:02', '1'),
(58, 'gfdfg', '2016-05-26 12:37:00', '2016-05-20 16:07:37', '1'),
(59, 'dewq', '2016-05-30 13:20:00', '2016-05-20 16:50:50', '1');

-- --------------------------------------------------------

--
-- Table structure for table `project_users`
--

CREATE TABLE IF NOT EXISTS `project_users` (
  `id` int(20) NOT NULL,
  `project_id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `video` varchar(250) DEFAULT NULL,
  `is_owner` enum('0','1') NOT NULL DEFAULT '0',
  `is_accept` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0 for pendding, 1 for accept,2 for reject'
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_users`
--

INSERT INTO `project_users` (`id`, `project_id`, `user_id`, `video`, `is_owner`, `is_accept`) VALUES
(23, 8, 26, '', '0', '0'),
(25, 9, 26, '', '0', '0'),
(26, 20, 15, '', '0', '0'),
(29, 8, 16, '', '0', '0'),
(31, 8, 16, '', '0', '0'),
(32, 8, 17, '', '0', '2'),
(33, 8, 18, '', '0', '0'),
(34, 8, 17, '', '0', '2'),
(35, 8, 18, '', '0', '1'),
(61, 9, 7, '2016-05-17_08:17_Untitled Document.qt', '1', '1'),
(62, 51, 33, '2016-05-16_01:25_Untitled Document.qt', '0', '1'),
(63, 8, 20, '', '0', '0'),
(64, 10, 26, '', '0', '0'),
(65, 10, 7, '', '1', '0'),
(66, 10, 23, '', '0', '0'),
(67, 10, 23, '', '0', '0'),
(68, 12, 26, NULL, '1', '0'),
(69, 13, 26, NULL, '1', '0'),
(70, 14, 26, NULL, '1', '0'),
(71, 15, 26, NULL, '1', '0'),
(72, 16, 26, NULL, '1', '0'),
(73, 17, 26, NULL, '1', '0'),
(74, 18, 26, NULL, '1', '0'),
(75, 19, 26, NULL, '1', '0'),
(76, 20, 26, NULL, '1', '0'),
(77, 21, 26, NULL, '1', '0'),
(78, 22, 26, NULL, '1', '0'),
(79, 23, 26, NULL, '1', '0'),
(80, 24, 26, NULL, '1', '0'),
(81, 25, 26, NULL, '1', '0'),
(82, 26, 26, NULL, '1', '0'),
(83, 27, 26, NULL, '1', '0'),
(84, 27, 26, NULL, '0', '0'),
(85, 28, 26, NULL, '1', '0'),
(86, 28, 26, NULL, '0', '0'),
(87, 29, 26, NULL, '1', '0'),
(88, 29, 26, NULL, '0', '0'),
(89, 9, 27, NULL, '0', '0'),
(92, 30, 26, NULL, '1', '0'),
(93, 30, 26, NULL, '0', '0'),
(94, 31, 26, NULL, '1', '0'),
(95, 32, 26, NULL, '1', '0'),
(96, 33, 26, NULL, '1', '0'),
(97, 34, 26, NULL, '1', '0'),
(98, 35, 26, NULL, '1', '0'),
(99, 36, 26, NULL, '1', '0'),
(100, 37, 26, NULL, '1', '0'),
(101, 38, 26, NULL, '1', '0'),
(102, 39, 26, NULL, '1', '0'),
(103, 40, 26, NULL, '1', '0'),
(104, 41, 26, NULL, '1', '0'),
(105, 42, 26, NULL, '1', '0'),
(106, 43, 26, NULL, '1', '0'),
(107, 43, 26, NULL, '0', '0'),
(108, 43, 28, NULL, '0', '0'),
(109, 44, 26, NULL, '1', '0'),
(110, 44, 26, NULL, '0', '0'),
(111, 44, 28, NULL, '0', '0'),
(112, 10, 29, NULL, '0', '0'),
(113, 10, 29, NULL, '0', '0'),
(114, 10, 29, NULL, '0', '0'),
(115, 45, 26, NULL, '1', '0'),
(116, 46, 26, NULL, '1', '0'),
(117, 47, 26, NULL, '1', '0'),
(118, 48, 26, NULL, '1', '0'),
(119, 49, 26, NULL, '1', '0'),
(120, 50, 26, NULL, '1', '0'),
(121, 51, 7, NULL, '1', '1'),
(122, 52, 26, NULL, '1', '0'),
(123, 53, 26, NULL, '1', '0'),
(124, 54, 26, NULL, '1', '0'),
(125, 55, 26, NULL, '1', '0'),
(126, 9, 23, NULL, '0', '0'),
(127, 56, 26, NULL, '1', '1'),
(128, 57, 26, NULL, '1', '1'),
(129, 58, 26, NULL, '1', '1'),
(130, 59, 26, NULL, '1', '1'),
(131, 10, 32, NULL, '0', '0'),
(132, 10, 33, NULL, '0', '0'),
(133, 10, 34, NULL, '0', '0'),
(134, 10, 35, NULL, '0', '0'),
(135, 10, 36, NULL, '0', '0'),
(137, 51, 23, NULL, '0', '0'),
(138, 51, 26, NULL, '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(200) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `user_type` enum('twitter','facebook','app') NOT NULL DEFAULT 'app',
  `email` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  `profile_image` varchar(200) NOT NULL,
  `contact_no` int(20) NOT NULL,
  `status` enum('active','pending_approval') DEFAULT NULL,
  `otp` varchar(10) NOT NULL,
  `otp_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_login` datetime NOT NULL,
  `is_searchable` enum('0','1') NOT NULL DEFAULT '1' COMMENT '1 for searchable, 0 for not'
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `user_type`, `email`, `password`, `profile_image`, `contact_no`, `status`, `otp`, `otp_created`, `last_login`, `is_searchable`) VALUES
(7, 'shubham', 'saini', 'app', 'shubham.intersoft@gmail.com', '4a2b626166dd06a245062d04e2fbd38e173974ed', '7.png', 0, 'active', '', '2016-04-26 12:04:09', '2016-05-18 10:27:50', '1'),
(10, 'abc', 'abc', 'app', 'abc@gmail.com', '9e582545a2e30821a6df997e04615ffa282e2874', '', 0, 'pending_approval', '956795', '2016-04-26 13:04:34', '0000-00-00 00:00:00', '1'),
(11, 'abc', 'abc', 'app', 'abc@gmail.com', '9e582545a2e30821a6df997e04615ffa282e2874', '', 0, 'pending_approval', '521462', '2016-04-26 13:04:34', '0000-00-00 00:00:00', '1'),
(19, 'shubham', 'saini', 'app', 'test@gmail.com', '12345', '', 0, 'active', '785104', '2016-05-13 08:05:13', '2016-05-13 13:32:38', '1'),
(20, '', '', 'app', 'test2@gmail.com', '', '', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(21, '', '', 'app', 'test5@gmail.com', '', '', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(22, 'shubham', 'saini', 'app', 'shubham1.intersoft@gmail.com', 'c378a21daac59e33c84dda64ac76fae361380d26', '', 0, 'pending_approval', '201511', '2016-05-13 08:05:12', '0000-00-00 00:00:00', '1'),
(23, '', '', 'app', 'abcdef@gmail.com', '', '', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(26, 'inder', 'arora', 'app', 'inder.intersoft@gmail.com', 'a68c83e01c249f71a29a4394cbeb6780e0b8f4d1', '2016-05-20_02:41_image', 0, 'active', '', '2016-05-13 13:05:22', '2016-05-20 14:42:14', '1'),
(27, '', '', 'app', 'tete1@gmail.com', '', '', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(28, '', '', 'app', 'shubhamsaini401@gmail.com', '', '', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(29, '', '', 'app', 'mr.shubhamsaini001@gmail.com', '', '', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(30, 'shubham', 'saini', 'app', 'shubham.intersoft2@gmail.com', 'c378a21daac59e33c84dda64ac76fae361380d26', '', 0, 'pending_approval', '171123', '2016-05-19 15:05:46', '0000-00-00 00:00:00', '1'),
(31, 'shubham', 'saini', 'app', 'utest266@gmail.com', 'c378a21daac59e33c84dda64ac76fae361380d26', '', 0, 'pending_approval', '516332', '2016-05-19 15:05:47', '0000-00-00 00:00:00', '1'),
(32, '', '', 'app', 'jadu@gmail.com', '', '', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(33, '', '', 'app', 'magic@gmail.com', '', '', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(34, 'jadu1', '', 'app', 'jadu1@gmail.com', '', '', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(35, 'magic1', '', 'app', 'magic1@gmail.com', '', '', 0, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(36, '', '', 'app', '12345@gmail.com', '', '', 0, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0'),
(37, 'shubham', 'saini', 'app', 'abcd@gmail.com', 'c378a21daac59e33c84dda64ac76fae361380d26', '', 0, 'pending_approval', 'fffbbb352e', '2016-06-22 10:06:59', '0000-00-00 00:00:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user_videos`
--

CREATE TABLE IF NOT EXISTS `user_videos` (
  `video_id` int(20) NOT NULL,
  `user_id` int(20) NOT NULL,
  `video_name` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_videos`
--

INSERT INTO `user_videos` (`video_id`, `user_id`, `video_name`) VALUES
(1, 7, '2016-05-04 07:18Desktop_002.png'),
(2, 7, '2016-05-04 07:24Workspace 1_001.png'),
(3, 7, '2016-05-04 07:24Workspace 1_001.png'),
(4, 7, '2016-05-04_07:39_Workspace 1_001.png'),
(5, 7, '2016-05-04_08:01_Workspace 1_001.png'),
(6, 7, '2016-05-04_08:01_Desktop_002.png'),
(7, 7, '2016-05-04_08:04_Untitled Document.mp4'),
(8, 7, '2016-05-05_09:48_Untitled Document.3gp'),
(9, 7, '2016-05-05_09:53_Untitled Document.qt'),
(10, 7, '2016-05-05_12:14_Untitled Document.qt'),
(11, 7, '2016-05-05_12:15_Untitled Document.qt');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_access_tokens`
--
ALTER TABLE `auth_access_tokens`
  ADD PRIMARY KEY (`access_token_id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`msg_id`);

--
-- Indexes for table `message_recipients`
--
ALTER TABLE `message_recipients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `project_users`
--
ALTER TABLE `project_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_videos`
--
ALTER TABLE `user_videos`
  ADD PRIMARY KEY (`video_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_access_tokens`
--
ALTER TABLE `auth_access_tokens`
  MODIFY `access_token_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `msg_id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `message_recipients`
--
ALTER TABLE `message_recipients`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `notification_id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `project_users`
--
ALTER TABLE `project_users`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=139;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `user_videos`
--
ALTER TABLE `user_videos`
  MODIFY `video_id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
