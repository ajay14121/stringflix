angular.module('stringflix.controllers').controller('homeCtrl', function ($cordovaFile, $scope, ionicToast, $interval, $stateParams, $http, $location, $state, $ionicPopup, $localStorage, $rootScope, $ionicLoading, $cordovaCapture, $cordovaCamera, $cordovaFileTransfer, video, $ionicActionSheet) {
    $scope.data = {};
    $scope.data.name = '';
    $scope.project_id = '';
    $scope.save = true;
    $scope.data.userList = [];
    $scope.data.fileURI = 'video uri';
    $scope.title = "New Project";
    $scope.disabled = false;
    $scope.readonly = false;
    $scope.videoURI = '';
    $scope.videoList = [];
    $scope.data.email = '';
    $scope.data.player_name = '';
    $scope.isSelectDeadline = false;
    $scope.data.deadline = new Date();
    $rootScope.invitedEmail = [];
    $scope.project_id=$stateParams.project_id;
    var counter = 0;
    try {
        video.reset();
    } catch (e) {
        console.log("exception in reset" + e);
    }
    $scope.invitePlayer = function () {
       $ionicPopup.alert({
            title: 'Insert Details',
            templateUrl: 'templates/popup/send_invite.html',
            scope: $scope,
            cssClass: 'send_invite',
            buttons: [
                {
                    text: "cancel"
                },
                {
                    text: "Ok",
                    onTap: function () {
                        if ($scope.data.email == '') {
                            $ionicPopup.alert({
                                title: 'Empty field',
                                template: "Email is mandatory",
                                 buttons:[
                                    {
                                       text:'ok',
                                      onTap:function(){
                                        $scope.invitePlayer();
                                    } 
                                    }
                                ]
                                
                            });
                            //$scope.invitePlayer();
                        } else if (!$rootScope.email_filter.test($scope.data.email)) {
                            $ionicPopup.alert({
                                title: 'Invalid Email',
                                template: 'Please enter valid email',
                                buttons:[
                                    {
                                       text:'ok',
                                      onTap:function(){
                                        
                                        $scope.invitePlayer();
                                    } 
                                    }
                                ]
                            });
                            
                        } else {
                             $scope.data.player_name= $scope.data.email.substr(0, $scope.data.email.indexOf('@'));
                            $ionicLoading.show();
                            $http({
                                url: $rootScope.apiUrl + "API/projects/invitation",
                                method: 'POST',
                                data: {
                                    "email": $scope.data.email,
                                    "project_id": $scope.project_id,
                                    "name": $scope.data.player_name
                                },
                                headers: {
                                    'Access-Token': $localStorage.user.token,
                                    'Email': $localStorage.user.email
                                }
                            }).success(function (response) {
                                $ionicLoading.hide();
                                if (response.status == '1')
                                {
                                    $ionicPopup.show({
                                        title: "Success",
                                        template: response.message,
                                        buttons: [
                                            {
                                                text: "ok",
                                                onTap: function () {
                                                    $state.go("home", {project_id: $stateParams.project_id}, {reload: true});
                                                }
                                            }
                                        ]
                                    });
                                } else {
                                    $ionicPopup.alert({
                                        title: "Failed",
                                        template: response.message
                                    });
                                }

                                                }).error(function (error) {
                                                    $ionicLoading.hide();
                                                    $rootScope.responseMessage("API Error", 'API is not working');
                                                });
                                            }
                                        }
                                    }
                                     
                                ]
                            });
    };
    $scope.showContacts = function () {
        if (!$scope.project_id)
        {
            $ionicPopup.alert({
                title: 'Invalid',
                template: 'Please select a project.'
            });
        } else {
            var showActionSheet = $ionicActionSheet.show({
                buttons: [
                    {text: 'Via Contacts'},
                    {text: 'Via Email'}
                ],
                titleText: 'Invite Player',
                cancelText: 'Cancel',
                buttonClicked: function (index) {
                    switch (index) {
                        case 0:
                            $state.go("invitations", {"project_id": $scope.project_id});
                            break;
                        case 1:
                            $scope.invitePlayer();
                            break;
                        default:
                            console.log("wrong choice");
                    }
                    return true;
                },
            });
        }
    };
    $scope.saveProject = function () {
        console.log($scope.data);
        var current_date_time = new Date();
        $scope.successloading=false;
        if ($scope.data.name == '') {
            $ionicPopup.alert({
                title: 'Invalid',
                template: 'Project name is mandatory.'
            });
        } else if (!$rootScope.name.test($scope.data.name)) {
            $ionicPopup.alert({
                title: 'Invalid',
                template: 'Only characters are allowed.'
            });
        } else if (!$scope.isSelectDeadline) {
            $ionicPopup.alert({
                title: 'Invalid',
                template: 'Project deadline is mandatory.'
            });
        } else if ($scope.data.deadline.getTime() <= current_date_time.getTime()) {
            $ionicPopup.alert({
                title: 'Invalid',
                template: 'Deadline should  be beyond current date and time.'
            });
        } else {
            $scope.data.deadline.toUTCString();
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/Projects/create",
                method: 'POST',
                data: {
                    project_name: $scope.data.name,
                    deadline: $scope.data.deadline,
                },
                headers: {
                    'Access-Token': $localStorage.user.token,
                    'Email': $localStorage.user.email
                }
            }).success(function (response) {
                $ionicLoading.hide();
                $scope.successloading=true;
                if (response.status == '1') {
                    $scope.project_id = response.data.project_id;
                    $state.go("home", {project_id: $scope.project_id});
                } else {
                    $ionicPopup.alert({
                        title: 'Failed',
                        template: response.message
                    });
                }
            }).error(function (erorr) {
                $ionicLoading.hide();
                $rootScope.responseMessage("API Error", 'API is not working.');
            });
        }
    };
    $scope.dateSelected = function ()
    {
        $scope.isSelectDeadline = true;
    };
    $scope.editProject = function () {
        var current_date_time = new Date();
        if ($scope.data.name == '') {
            $ionicPopup.alert({
                title: 'Invalid',
                template: 'Project name is mandatory.'
            });
        } else if (!$rootScope.name.test($scope.data.name)) {
            $ionicPopup.alert({
                title: 'Invalid',
                template: 'Only characters are allowed.'
            });
        } else if (!$scope.isSelectDeadline) {
            $ionicPopup.alert({
                title: 'Invalid',
                template: 'Project deadline is mandatory.'
            });
        } else if ($scope.data.deadline.getTime() <= current_date_time.getTime()) {
            $ionicPopup.alert({
                title: 'Invalid',
                template: 'Deadline should  be beyond current date and time.'
            });
        } else {
            $scope.data.deadline.toUTCString();

            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/Projects/update_project",
                method: 'POST',
                data: {
                    project_id: $stateParams.project_id,
                    name: $scope.data.name,
                    deadline: $scope.data.deadline,
                },
                headers: {
                    'Access-Token': $localStorage.user.token,
                    'Email': $localStorage.user.email
                }
            }).success(function (response) {
                $ionicLoading.hide();
                if (response.status == '1') {
                    $ionicPopup.alert({
                        title: 'Success',
                        template: response.message
                    });
                    $state.go("home", {project_id: $stateParams.project_id});
                } else {
                    $ionicPopup.alert({
                        title: 'Failed',
                        template: response.message
                    });
                }
            }).error(function (erorr) {
                $ionicLoading.hide();
                $rootScope.responseMessage("API Error", 'API is not working.');
            });
        }
    }
    $scope.countUnread = function () {

        $rootScope.badgeInterval = $interval(function () {
            if ($localStorage.user != undefined) {
                $http({
                    url: $rootScope.apiUrl + "API/user/count",
                    method: 'POST',
                    headers: {
                        'Access-Token': $localStorage.user.token,
                        'Email': $localStorage.user.email
                    }
                }).success(function (response) {
                    if (response.status == '1') {
                        $rootScope.notificationCount = response.data.notification_count;
                        $rootScope.messageCount = response.data.msg_count;

                    } else {
                        console.log(response.message);
                    }
                }).error(function (erorr) {
                    $rootScope.responseMessage("API Error", 'API is not working.');
                });
            }
        }, 2000);

    };
    $scope.deleteFlix = function () {
        if ($scope.is_owner == "0")
        {
            $ionicPopup.alert({
                title: 'Invalid Action',
                template: "you do not have permission to delete this project."
            });
        } else {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Delete Flix',
                template: 'Are you sure you want to delete flix?'
            });

            confirmPopup.then(function (res) {
                if (res) {
                    $ionicLoading.show();
                    $http({
                        url: $rootScope.apiUrl + "API/projects/delete_project",
                        method: 'POST',
                        data: {
                            project_id: $scope.project_id
                        },
                        headers: {
                            'Access-Token': $localStorage.user.token,
                            'Email': $localStorage.user.email
                        }
                    }).success(function (response) {
                        $ionicLoading.hide();
                        if (response.status == '1') {
                            $ionicPopup.alert({
                                title: 'Success',
                                template: "you have successfully deleted a project.",
                                buttons:[
                                    {
                                        text:'ok',
                                        onTap:function(){
                                            $state.go("projects");
                                        }
                                    }
                                ]
                            });
                        } else {
                            $ionicPopup.alert({
                                title: 'Failed',
                                template: response.message
                            });
                        }
                    }).error(function (erorr) {
                        $ionicLoading.hide();
                        $rootScope.responseMessage("API Error", 'API is not working');
                    });
                } else {
                    console.log('You are not sure');
                }
            });
        }
    };
    $scope.$on('$ionicView.enter', function () {
        $scope.countUnread();
        $scope.videos = [];
        $scope.videoList = [];
        $scope.successloading=false;
        //alert("hello"+$stateParams.project_id+"abc");

        if ($stateParams.project_id > 0) {
            $scope.project_id = $stateParams.project_id;
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/projects/project_details",
                method: 'POST',
                data: {
                    project_id: $stateParams.project_id
                },
                headers: {
                    'Access-Token': $localStorage.user.token,
                    'Email': $localStorage.user.email
                }
            }).success(function (response) {
                $ionicLoading.hide();
                $scope.successloading=true;
                if (response.status == '1') {
                    $scope.data.name = response.data.project_name;
                    $scope.data.deadline = new Date(response.data.deadline);

                    $scope.title = $scope.data.name;
                    $scope.data.userList = response.data.invited_users;
                    $scope.is_owner = response.data.is_owner;
                    $scope.save = false;
                    for (var index = 0; index < $scope.data.userList.length; index++)
                    {
                        $rootScope.invitedEmail.push($scope.data.userList[index].email);

                    }
                    for (var index = 0; index < $scope.data.userList.length; index++)
                    {
                        console.log($scope.data.userList[index].name);
                        if ($scope.data.userList[index].name != ' ')
                        {
                            var ss = {};
                            ss.video = $scope.data.userList[index].video;
                            ss.name = $scope.data.userList[index].name;
                            $scope.videos.push(ss);
                        }
                        // alert("length video"+$scope.videos.length);
                        //alert("list of video owner"+JSON.stringify($scope.videos));
//                        alert("object of video"+JSON.stringify($scope.videos));
                    }
                    $scope.downloadFiles($scope.videos);
                    if ($scope.is_owner == "0" || $rootScope.history_project == '1')
                    {
                        $scope.readonly = true;
                    }
                    
                    // Timer run if deadline of project will not reach
                    if ($rootScope.history_project != '1') {
                        $scope.data.deadline_old = $scope.data.deadline;
                        $scope.deadlineTimer = $interval(function () {
                            var diff;
                            diff = Math.floor(($scope.data.deadline_old.getTime() - new Date().getTime()) / 1000);

                            if (diff <= 0) {
                                // Redirect

                                $interval.cancel($scope.deadlineTimer);
                                $ionicPopup.alert({
                                    title: 'Alert',
                                    template: "Oops Project Deadline Reached",
                                    buttons: [
                                        {
                                            text: 'ok',
                                            onTap: function () {

                                                $state.go("project_history", {reload: true});
                                            }
                                        }
                                    ]
                                });
                            }
                        }, 1000);
                    }
                    

                } else {
                    console.log("no project found");
                }
            }).error(function (erorr) {
                $ionicLoading.hide();

                $rootScope.responseMessage("API Error", 'API is not working.');
            });

        } else {
            $scope.is_owner = '1';
            $scope.successloading=true;
            $rootScope.history_project = '0';
        }
    });
     $scope.$on('$ionicView.leave', function () {
        
         $interval.cancel($scope.deadlineTimer);
    });
    $scope.downloadFiles = function (videos) {

        var progress_count = 0;
        $scope.dynamicProgress = 'progress-0';
        $scope.progressStatus=0;
        if(counter<videos.length){
        if(videos[counter].video==''){
            $scope.videoList.push({"src":' ', "name": videos[counter].name,"isSrc":false});
            counter++;
            $scope.downloadFiles(videos);
        }else{
        var directory_path = '';
        console.log("first index" + counter);
        try {
            if ($rootScope.platform == 'android' || $rootScope.platform == 'Android') {
                directory_path = cordova.file.externalDataDirectory;
            } else {
                directory_path = cordova.file.dataDirectory;
            }
            var checkDirectory = $cordovaFile.createDir(directory_path, $scope.project_id, false);
            var filename = videos[counter].video.substring(videos[counter].video.lastIndexOf("/") + 1, videos[counter].video.length);
//                 alert("index before download" + counter);
            var path = directory_path + $scope.project_id + '/' + filename;
            var type=filename.substring(filename.length-4,filename.length);
            $cordovaFile.checkFile(directory_path + $scope.project_id + '/', filename)
                    .then(function (success) {

                        if (counter < videos.length)
                        {
                            video.addSource(type, success.nativeURL);
                            $scope.videoList.push({"src": success.nativeURL, "name": videos[counter].name,"isSrc":true});
                            counter++;
                            $scope.downloadFiles(videos);

                        }
                    }, function (error) {
                        if (counter < videos.length)
                        {
                            $cordovaFileTransfer.download(videos[counter].video, path, {})
                                    .then(function (result) {
                                        $ionicLoading.hide();
                                        $scope.popup.close();
                                        var successPopup=$ionicPopup.alert({
                                            title: 'Success',
                                            template: filename + " downloaded successfully",
                                            buttons: [
                                                {
                                                    text: "Ok",
                                                    onTap: function () {
                                                        successPopup.close();
                                                        $scope.downloadFiles(videos);
                                                    }
                                                }
                                            ]
                                        })
                                        video.addSource(type, result.nativeURL);

                                        $scope.videoList.push({"src": result.nativeURL, "name": videos[counter].name,"isSrc":true});
                                        counter++;

                                        // Success!
                                    }, function (err) {
                                        console.log("DGFGFD" + JSON.stringify(err));
                                        $ionicLoading.hide();
                                        // Error
                                    }, function (progress) {
                                        $ionicLoading.hide();
                                        $scope.progressStatus = Math.round((progress.loaded / progress.total) * 100);
                                        if (progress_count == 0)
                                        {
                                            $scope.popup = $ionicPopup.show({
                                                title: 'downloading ' + filename,
                                                templateUrl: 'templates/popup/progress.html',
                                                delay: '100',
                                                scope: $scope,
                                            });
                                        }
                                        if ($scope.progressStatus % 5 == 0)
                                        {
                                            $scope.dynamicProgress = 'progress-' + $scope.progressStatus;
                                        }

                                        progress_count++;
                                        // constant progress updates
                                    });
                        }
                    });


        } catch (e) {
            console.log('File Transfer Plugin will work on device.');
            console.log(e);
        }
    }
        }
    };
    $scope.setVideo = function () {
        var showActionSheet = $ionicActionSheet.show({
                buttons: [
                    {text: 'via Camera'},
                    {text: 'Via Gallery'}
                ],
                titleText: 'Upload Your Video',
                cancelText: 'Cancel',
                buttonClicked: function (index) {
                    switch (index) {
                        case 0:
                             try {
                            var options = {limit: 1, quality: 1};

                            $cordovaCapture.captureVideo(options).then(function (videoData) {
                                $scope.videoURI = videoData[0].fullPath;
                                $scope.uploadVideo();
                            }, function (err) {
                                // An error occurred. Show a message to the user
                            });
                        } catch (e) {
                            console.log("video capture will work on device only" + e);
                        }
                            break;
                        case 1:
                              var options = {
                            quality: 50,
                            destinationType: Camera.DestinationType.FILE_URI, // <== try THIS
                            sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
                            mediaType: navigator.camera.MediaType.VIDEO
                        };

                        $cordovaCamera.getPicture(options).then(function (videoURI) {
                            $scope.videoURI = videoURI;
                            $scope.uploadVideo();
                        }, function (err) {
                            console.log("err", JSON.stringify(err));
                        });
                            break;
                        default:
                            console.log("wrong choice");
                    }
                    return true;
                },
            });

    };
    
    $scope.uploadVideo = function () {
        var progress_count = 0;
        $scope.dynamicProgress = 'progress-0';
        $scope.progressStatus = 0;
        var server = $rootScope.apiUrl + "API/user/upload_video";
        //var filePath = cordova.file.documentsDirectory + "testImage.png";

        var filePath = $scope.videoURI;
        var trustHosts = true;
        var fileName = 'video.mov';
        var mimeType = "video/quicktime";

        if ($rootScope.platform == 'Android' || $rootScope.platform == 'android') {
            fileName = 'video.mp4';
            mimeType = "video/mp4";
        }
        var options = {
            params: {
                type:'file',
                video: $scope.videoURI,
                project_id: $scope.project_id
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email,
            },
            fileKey: 'video',
            fileName: fileName,
            mimeType: mimeType,
        };
        try {
            $ionicLoading.show();
            $cordovaFileTransfer.upload(server, filePath, options)
                    .then(function (result) {
                        result = angular.fromJson(result);
                        response = angular.fromJson(result.response);

                        if (response.status == "1") {
                            $ionicLoading.hide();
                            $scope.data.fileURI = response.data.url;
                            $scope.popup.close();
                            $ionicPopup.alert({
                                title: 'Success',
                                template: "you have successfully uploaded a video",
                                buttons: [
                                    {
                                        text: 'ok',
                                        onTap: function () {
                                            $state.go("home", {project_id: $scope.project_id}, {reload: true});
                                        }
                                    }
                                ]
                            });
                        } else {
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Failed',
                                template: response.message
                            });
                        }
                        // Success!
                    }, function (err) {
                        $ionicLoading.hide();
                        $rootScope.responseMessage("API Error", 'API is not working.');
                        // Error
                    }, function (progress) {
                        $ionicLoading.hide();
                        $scope.progressStatus = Math.round((progress.loaded / progress.total) * 100);
                        if (progress_count == 0)
                        {
                            $scope.popup = $ionicPopup.show({
                                title: 'uploading',
                                templateUrl: 'templates/popup/progress.html',
                                delay: '100',
                                scope: $scope,
                            });
                            progress_count++;
                        }
                        if ($scope.progressStatus % 5 == 0)
                        {
                            $scope.dynamicProgress = 'progress-' + $scope.progressStatus;
                        }
                    });

        } catch (e) {
            console.log('File Transfer Plugin will work on device.');
            console.log(e);
        }
    };
    $scope.deleteUser = function (user_id, name, index) {
        if ($rootScope.history_project == '0')
        {
            if ($scope.is_owner == "0")
            {
                $ionicPopup.alert({
                    title: 'Delete User',
                    template: "you do not have permission to delete users."
                });
            } else if (user_id == $localStorage.user.id)
            {
                $ionicPopup.alert({
                    title: 'Delete User',
                    template: "owner cannot be deleted."
                });
            } else {
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Delete User',
                    template: 'Are you sure you want to delete ' + name + '?'
                });

                confirmPopup.then(function (res) {
                    if (res) {
                        $ionicLoading.show();
                        $http({
                            url: $rootScope.apiUrl + "API/projects/delete_user",
                            method: 'POST',
                            data: {
                                project_id: $scope.project_id,
                                user_id: user_id
                            },
                            headers: {
                                'Access-Token': $localStorage.user.token,
                                'Email': $localStorage.user.email
                            }
                        }).success(function (response) {
                            $ionicLoading.hide();
                            if (response.status == '1') {
                                $scope.data.userList.splice(index, 1);
                                $ionicPopup.alert({
                                    title: "Success",
                                    template: "you have deleted" + name
                                });

                                $rootScope.invitedEmail.splice(0, $rootScope.invitedEmail.length);
                                for (var i = 0; i < $scope.data.userList.length; i++)
                                {
                                    $rootScope.invitedEmail.push($scope.data.userList[i].email);
                                }

                            } else {
                                $ionicPopup.alert({
                                    title: 'Failed',
                                    template: response.message
                                });
                            }
                        }).error(function (erorr) {
                            $ionicLoading.hide();
                            $rootScope.responseMessage("API Error", 'API is not working.');
                        });
                    } else {
                        console.log('You are not sure');
                    }
                });

            }
        }
    };
    $rootScope.isOldDate = function (new_date) {


        date_time = new Date();

        if (date_time.getTime() < new_date.getTime()) {
            return true;

        } else {
            $ionicPopup.alert({
                title: "Failed",
                template: "Deadline cannot be lesser than current date"
            });
            return false;

        }
    };
}).directive('countdown', [
    'Util',
    '$interval',
    function (Util, $interval) {
        return {
            restrict: 'A',
            scope: {date: '@'},
            link: function (scope, element) {
                var future;
                
                var deadlineTimer=$interval(function () {
                    var diff;
                    future = new Date(scope.date);
                    diff = Math.floor((future.getTime() - new Date().getTime()) / 1000);
                      if(diff==0)
                      {
                          $interval.cancel(deadlineTimer);
                      }
                    return element.text(Util.dhms(diff));
                }, 1000);
            }
        };
    }
]).factory('Util', [function () {
        return {
            dhms: function (t) {
                var days, hours, minutes, seconds;
                days = Math.floor(t / 86400);
                t -= days * 86400;
                hours = Math.floor(t / 3600) % 24;
                t -= hours * 3600;
                minutes = Math.floor(t / 60) % 60;
                t -= minutes * 60;
                seconds = t % 60;
                return [
                    days + 'd',
                    hours + 'h',
                    minutes + 'm',
                    seconds + 's'
                ].join(' ');
            }
        };
    }]);
    