angular.module('stringflix.controllers').controller('invitesCtrl', function($scope, $state, $http, $rootScope, $cordovaContacts, $stateParams, $localStorage, $ionicLoading, $ionicPopup,ionicToast) {
    $scope.success=false;
    $scope.invitesLoad = function(){
        $ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/projects/invites",
            method: 'POST',
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            $ionicLoading.hide();
            if (response.status == '1') {
                $scope.success=true;
                console.log(response.data);
                $scope.invites =  response.data;
                console.log($scope.invites);
                if(response.data.length==0)
                {
                    $ionicPopup.alert({
                        title: "Empty List",
                        template: "No invitations for you"
                     });
                }
            } else {
                 $ionicPopup.alert({
                        title: "Empty",
                        template: response.message
                     });
            }
        }).error(function(erorr) {
            $ionicLoading.hide();
            $rootScope.responseMessage('Api Error','API is not working');
        });
    };
    
    $scope.acceptRejectInvite = function(invitation_id,project_id,status){
        $ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/projects/update_users",
            method: 'POST',
            data:{
                'invitation_id':invitation_id,
                'status':status
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            $ionicLoading.hide();
            if (response.status == '1') {
                console.log(project_id);
                if(status==1)
                {
                    $rootScope.history_project=='0';
                    $state.go("home",{project_id:project_id},{reload:true});
                }else{
                    $scope.invitesLoad();
                }
                
                
            } else {
		$rootScope.responseMessage('Failed',response.message);     
            }
        }).error(function(erorr) {
            $ionicLoading.hide();
            $rootScope.responseMessage('Api Error','API is not working');
        });
    };
    $scope.invitesLoad();
});

