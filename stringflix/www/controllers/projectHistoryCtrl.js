angular.module('stringflix.controllers').controller('projectHistoryCtrl', function($scope,$timeout,$localStorage,$stateParams,$interval, $http, $state, $ionicPopup, $rootScope, $ionicLoading,$ionicScrollDelegate,ionicToast) {
    $scope.$on('$ionicView.enter', function() {
         $rootScope.history_project='0';
         $scope.isRecordFound=false;
         $scope.success=false;
         $scope.loadProjects();
        
    });
    $scope.loadProjects=function(){
        $ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/Projects/list",
            method: 'POST',
            data:{
                is_history:1
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            $ionicLoading.hide();
            $scope.success=true;
            if (response.status == '1') {
                $scope.project_list =  response.data;
                $scope.isRecordFound=true;
            } else {
                $scope.isRecordFound=false;
            }
        }).error(function(erorr) {
            $ionicLoading.hide();
            $rootScope.responseMessage("API Error", 'API is not working');
        });
    };
    $scope.projectDetail=function(id,ownership){
        $rootScope.history_project='1';
        $state.go("home",{project_id:id,is_owner:ownership});
    };
});
    