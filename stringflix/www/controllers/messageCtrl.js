angular.module('stringflix.controllers').controller('messageCtrl', function($scope,$state, $timeout,$ionicPopup, $localStorage, $stateParams, $interval, $http, $location, $rootScope, $ionicLoading, $ionicScrollDelegate,ionicToast) {
    $scope.data = {};
    $scope.data.message='';
    $scope.hideTime = true;
    $scope.user_messages = [];
    $scope.myInterval = '';
    $scope.message_unread='';
    $scope.isRecordFound=false;
    $scope.success=false;
    
    $scope.title = $rootScope.project_title;
    $scope.$on('$ionicView.enter', function() {
        $scope.messageCount=0;
        $scope.messagesLoad();
        $scope.myInterval = $interval(function() {
            $scope.messagesLoad();
        }, 500);
    });
    $scope.$on('$ionicView.leave', function() {
        $interval.cancel($scope.myInterval);
    });
    $scope.messagesLoad = function() {
        console.log("message loading");
        console.log($stateParams.project_id);
        $scope.message_unread='';
        $http({
            url: $rootScope.apiUrl + "API/message/list",
            method: 'POST',
            data: {
                project_id: $stateParams.project_id
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            $scope.success=true;
            console.log(response.status);
                 if (response.status == '1') {
                old_messages = $scope.messages;
                $scope.messages = response.data; 
                $scope.isRecordFound=true;
                $scope.message_unread='';
                if (response.data.length != old_messages.length) {
                    $ionicScrollDelegate.scrollBottom(true);
                }
                console.log($scope.messages.length);
                for(var index=0;index<$scope.messages.length;index++)
                {
                    console.log($scope.messages[index].is_read);
                    var created = new Date($scope.messages[index].created);
                    $scope.messages[index].created =  created.toLocaleString();
                    
                    if($scope.messages[index].is_read=="0")
                    {
                        $scope.message_unread+=$scope.messages[index].msg_id+',';
                    }
                }
                console.log($scope.message_unread);
                  $scope.changeMsgStatus($scope.message_unread);
            } else {
                if($scope.messageCount==0){
                    $scope.isRecordFound=false;
                    $scope.messageCount++;
                }
               
           }
              
        }).error(function(erorr) {
            $rootScope.responseMessage("API Error",'API is not working');
        });
    };



    $scope.sendMessage = function() {
        if ($scope.data.message.trim() == '') {
            return false;
        }
        //$ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/Message/send",
            method: 'POST',
            data: {
                type: 'group',
                message: $scope.data.message,
                send_to: $stateParams.project_id
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            //$ionicLoading.hide();
            if (response.status == '1') {
                $scope.data.message='';
                $scope.messagesLoad();
                
            } else {
                $ionicPopup.alert({
                             title: 'Failed',
                             template: response.message
                       });
            }
        }).error(function(erorr) {
            //$ionicLoading.hide();
            $rootScope.responseMessage("API Error",'API is not working');
        });
    };
    $scope.changeMsgStatus=function(message_unread)
    {
        message_unread=message_unread.substring(0,message_unread.length-1);
   
            //$ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/Message/update",
            method: 'POST',
            data: {
                msg_id:message_unread,
                status:'1'
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            //$ionicLoading.hide();
            if (response.status == '1') {
                console.log("status changed successfully");
            } else {
                console.log("status change failed")
            }
        }).error(function(erorr) {
            //$ionicLoading.hide();
            console.log("API is not working")
        });
        
    };

    $scope.inputUp = function() {
        if ($rootScope.platform == "iOS" )
            
            $scope.data.keyboardHeight = 216;
        $timeout(function() {
            $ionicScrollDelegate.scrollBottom(true);
        }, 100);

    };

    $scope.inputDown = function() {
        if ($rootScope.platform == "iOS")
            $scope.data.keyboardHeight = 0;
        $ionicScrollDelegate.resize();
    };

    $scope.closeKeyboard = function() {
        // cordova.plugins.Keyboard.close();
    };


    $scope.data = {};
    $scope.myId = $localStorage.user.id;
    $scope.messages = [];

});


angular.module('stringflix').directive('input', function($timeout) {
    return {
        restrict: 'E',
        scope: {
            'returnClose': '=',
            'onReturn': '&',
            'onFocus': '&',
            'onBlur': '&'
        },
        link: function(scope, element, attr) {
            element.bind('focus', function(e) {
                if (scope.onFocus) {
                    $timeout(function() {
                        scope.onFocus();
                    });
                }
            });
            element.bind('blur', function(e) {
                if (scope.onBlur) {
                    $timeout(function() {
                        scope.onBlur();
                    });
                }
            });
            element.bind('keydown', function(e) {
                if (e.which == 13) {
                    if (scope.returnClose)
                        element[0].blur();
                    if (scope.onReturn) {
                        $timeout(function() {
                            scope.onReturn();
                        });
                    }
                }
            });
        }
    }
});
    