angular.module('stringflix.controllers').controller('messagesCtrl', function($scope, $http, $location, $ionicPopup, $rootScope, $ionicLoading,$ionicScrollDelegate,$localStorage) {
    $scope.data = {};
    $rootScope.project_title='';
    $scope.isRecordFound=false;
    $scope.success=false;
    $scope.messagePage = function(project_id,project_name){
        $location.path("/message/"+project_id);
        $rootScope.project_title=project_name;
    };
    $scope.loadProjects = function(){
        $ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/Projects/list",
            method: 'POST',
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            $ionicLoading.hide();
            $scope.success=true;
            if (response.status == '1') {
                $scope.projects =  response.data;
                $scope.isRecordFound=true;
            } else {
                $scope.isRecordFound=false;
            }
        }).error(function(erorr) {
            $ionicLoading.hide();
            $rootScope.responseMessage('Api Error','API is not working');
        });
    }
    $scope.loadProjects();
    
    
});


