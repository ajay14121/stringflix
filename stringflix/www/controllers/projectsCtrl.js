angular.module('stringflix.controllers').controller('projectsCtrl', function($scope,$timeout,$localStorage,$stateParams,$interval, $http, $state, $ionicPopup, $rootScope, $ionicLoading,$ionicScrollDelegate,ionicToast) {
    $scope.$on('$ionicView.enter', function() {
        $scope.isRecordFound=false;
    $scope.success=false;
     $rootScope.history_project=='0';
         $scope.dynamicClass='active';
         $scope.loadProjects();
        
    });
    $scope.projectList=[];
    $scope.loadProjects=function(){
        
        $ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/Projects/list",
            method: 'POST',
            data:{
                is_history:"2"
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            $ionicLoading.hide();
            $scope.success=true;
            if (response.status == '1') {
                $scope.project_list =  response.data;
                $scope.isRecordFound=true;
            } else {
                $scope.isRecordFound=false;
                console.log("no records found");
            }
        }).error(function(erorr) {
            $ionicLoading.hide();
            $rootScope.responseMessage("API Error", 'API is not working');
        });
    };
  
    $scope.display=function(type) {

        if(type=='')
        {
            $scope.dynamicClass='active';
        }else{
            $scope.dynamicClass='';
            
        }
        $scope.type = type;
    };
    $scope.projectDetail=function(id,ownership){
        
        $state.go("home",{project_id:id,is_owner:ownership});
    };
});
    