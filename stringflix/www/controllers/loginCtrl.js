angular.module('stringflix.controllers').controller('loginCtrl', function ($scope, $http, $location,$interval, $state, $ionicPopup, $rootScope, $ionicLoading, $localStorage, $cordovaFacebook,$ionicHistory) {
    $scope.data = {};
    $scope.$on('$ionicView.beforeEnter', function() {
              $scope.resend = false;
       });
    
    $scope.login = function () {
        if ($scope.data.email == '' || $scope.data.password == '') {
            $ionicPopup.alert({
                title: 'Empty field',
                template: 'Email and password are mandatory'
            });
        } else if (!$rootScope.email_filter.test($scope.data.email)) {
            $ionicPopup.alert({
                title: 'Invalid Email',
                template: 'Please enter valid email'
            });
        } else {
            console.log($scope.data.email);
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/User/login",
                method: 'post',
                data: {
                    email: $scope.data.email,
                    password: $scope.data.password,
                    uuid: $rootScope.uuid,
                    platform: $rootScope.platform,
                    build:$rootScope.build
                }
            }).success(function(response) {
                $ionicLoading.hide();
                console.log(response.data.length);
                
                if(response.status == '1') {
                    $localStorage.user = response.data;
                    $rootScope.user=$localStorage.user;
                    if (response.data.length == 0)
                    {
                        $ionicPopup.alert({
                            title: "Login Failed",
                            template: "Please verify your account through email",
                            buttons: [
                                {
                                    text: 'ok',
                                    onTap: function () {
                                        $scope.resend = true;
                                    }
                                }
                            ]
                        });
                    }else if(response.data.status == 'active') {
                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        $state.go("home", null, {reload: true});
                    } 
                } else {
                    $ionicPopup.alert({
                title: 'Login Failed',
                template: response.message
            });
                }
            }).error(function(error) {
                $ionicLoading.hide();
                console.log(error);
                $rootScope.responseMessage('Api Error','API is not working');
            });

        }
    };
    $scope.resendVerificationLink = function () {
        $ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/User/send_otp",
            method: 'post',
            data: {
                email: $scope.data.email
            }
        }).success(function (response) {
            $ionicLoading.hide();

            if (response.status == '1') {
                $ionicPopup.alert({
                    title: 'Success',
                    template: response.message
                });
            } else {
                $ionicPopup.alert({
                    title: 'Sending Failed',
                    template: response.message
                });
            }
        }).error(function (error) {
            $ionicLoading.hide();
            console.log(error);
            $rootScope.responseMessage('Api Error', 'API is not working');
        });
    }
});
    