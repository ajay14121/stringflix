// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('stringflix', ['ionic', 'stringflix.controllers', 'ion-datetime-picker', 'ngCordova', 'ngStorage','ngVideo','ionic-toast'])
        .run(function ($ionicPlatform, $rootScope,$state, $localStorage, $cordovaDevice,$ionicHistory,ionicToast,$cordovaPush) {
            $ionicPlatform.ready(function () {
                $rootScope.apiUrl = "http://10.10.10.83/projects/bitbucket/stringflix/stringflix_api/index.php/";
                $rootScope.apiUrl = "http://giftsoninternet.com/all/stringflix/index.php/";
                $rootScope.apiUrl = "http://appliedsolar.com.au/all/stringflix/index.php/";
                $rootScope.build = "test"; // test or live
                $rootScope.history_project='0';
                // Set uuid & platform work only on device
                try {
                    $rootScope.uuid = $cordovaDevice.getUUID();
                    $rootScope.platform = $cordovaDevice.getPlatform();
                } catch (e) {
                    $rootScope.uuid = '';
                    $rootScope.platform = 'browser';
                }
                // Redirect on previous page
                $rootScope.myGoBack = function() {
                    $ionicHistory.goBack();
                };
                $rootScope.newProject= function(){
                    $state.go("home",{project_id:''},{reload:true})
                }
                // Offline userLogin Count

               $rootScope.pushIOS = function(){
                    try {
                        var iosConfig = {
                            "badge": true,
                            "sound": true,
                            "alert": true,
                        };

                        $cordovaPush.register(iosConfig).then(function(deviceToken) {
                            // Success -- send deviceToken to server, and store for future use
                            $rootScope.uuid = deviceToken;
                            console.log("Successfully iOS Device Registered for PushNotification ");
                            console.log("deviceToken: " + deviceToken);
                        }, function(err) {
                            console.log("Failed iOS Device Registered for PushNotification ");
                            console.log("Registration error: " + err);
                        });
                    } catch (e) {
                        console.log(e);
                    }
               };     
                
                $rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
                    if($rootScope.platform == 'android' || $rootScope.platform == 'Android' ){
                        switch (notification.event) {
                            case 'registered':
                                if (notification.regid.length > 0) {
                                    $rootScope.uuid = notification.regid;
                                    console.log('registration ID = ' + notification.regid);
                                }
                                break;

                            case 'message':
                                // this is the actual push notification. its format depends on the data model from the push server
                                console.log('message = ' + notification.message + ' msgCount = ' + notification.msgcnt);
                               console.log("type of notification"+notification.type);
//                                alert("project_id"+notification.project_id);
                                if(notification.payload.type=='invited')
                                {
                                    $state.go("invites");
                                }else if(notification.payload.type=='message'){
                                  $state.go("message",{project_id:notification.payload.project_id});
                                }
                                break;

                            case 'error':
                                console.log('GCM error = ' + notification.msg);
                                break;

                            default:
                                console.log('An unknown GCM event has occurred');
                                break;
                        }
                    } else {
                        if (notification.alert) {
                            //  navigator.notification.alert(notification.alert);
                             if(notification.type=='invited')
                                {
                                    $state.go("invites");
                               
                                }else if(notification.type=='message'){
                                  $state.go("message",{project_id:notification.project_id});
                                }
                        }

                        if (notification.sound) {
                            //  var snd = new Media(event.sound);
                            // snd.play();
                        }

                        if (notification.badge) {
                            $cordovaPush.setBadgeNumber(notification.badge).then(function(result) {
                                // Success!
                            }, function(err) {
                                // An error occurred. Show a message to the user
                            });
                        }
                    }
                });
                
                $rootScope.pushAndroid = function() {
                    try {
                        var androidConfig = {
                            "senderID": "764487091337"
                        };
                        $cordovaPush.register(androidConfig).then(function(result) {
                            // Success
                            console.log("Successfully Android Device Registered for PushNotification ");
                        }, function(err) {
                            // Error
                             console.log("Failed Android Device Registered for PushNotification ");
                        });

                        
                    } catch (e) {
                        console.log(e);
                    }
                };

                if($rootScope.platform == 'android' || $rootScope.platform == 'Android' ){
                    $rootScope.pushAndroid();
                } else {
                    $rootScope.pushIOS();
                }
                
                $rootScope.email_filter = /^[a-zA-Z0-9._]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                $rootScope.name= /^[a-zA-Z\s]*$/;
                $rootScope.digit= /^[0-9]+$/;
                $rootScope.item_added= /^[a-zA-Z0-9 ]*$/;
                $rootScope.responseMessage=function(title,message)
                {
                    //ionicToast.show(title+' '+message, 'bottom', true, 1000);
                }

                if (window.cordova && window.cordova.plugins.Keyboard) {
                    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                    // for form inputs)
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                    // Don't remove this line unless you know what you are doing. It stops the viewport
                    // from snapping when text inputs are focused. Ionic handles this internally for
                    // a much nicer keyboard experience.
                    cordova.plugins.Keyboard.disableScroll(true);
                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }

            });
        })
        .config(function ($stateProvider, $urlRouterProvider, $httpProvider, $localStorageProvider,$ionicConfigProvider) {
            $ionicConfigProvider.tabs.position('bottom');
            $stateProvider
                    .state('app', {
                        url: '/app',
                        abstract: true,
                        templateUrl: 'templates/menu.html',
                        controller: 'menuCtrl'
                    })
                    .state('login', {
                        url: '/login',
                        templateUrl: 'templates/login.html',
                        controller: 'loginCtrl'

                    })
                    .state('setting', {
                        url: '/setting',
                        templateUrl: 'templates/setting.html',
                        controller: 'settingCtrl'
                    })
                    .state('main', {
                        url: '/main',
                        templateUrl: 'templates/main.html',
                        controller: 'mainCtrl'
                    })
                    .state('home', {
                        url: '/home/:project_id',
                        templateUrl: 'templates/home.html',
                        controller: 'homeCtrl',
                        cache:false
                      
                    })
                     .state('invitations', {
                        url: '/invitations/:project_id',
                        templateUrl: 'templates/invitation.html',
                        controller: 'invitationCtrl',
                        cache:false
                    })
                    .state('invites', {
                        url: '/invites',
                        templateUrl: 'templates/invites.html',
                        controller: 'invitesCtrl',
                        cache:false
                    })
                    .state('my-profile', {
                        url: '/my-profile',
                        templateUrl: 'templates/my-profile.html',
                        controller: 'myProfileCtrl',
                        cache:false
                      
                    })
                    .state('forget_password', {
                        url: '/forget_password',
                        templateUrl: 'templates/forget_password.html',
                        controller: 'forgetPasswordCtrl',
                    })
                    .state('change_password', {
                        url: '/change_password',
                        templateUrl: 'templates/change_password.html',
                        controller: 'changePasswordCtrl'
                    })
                    .state('signup', {
                        url: '/signup',
                        templateUrl: 'templates/signup.html',
                        controller: 'signupCtrl'
                    })
                    .state('otp', {
                        url: '/otp',
                        templateUrl: 'templates/otp.html',
                        controller: 'otpCtrl'
                    })
                    .state('search', {
                        url: '/search',
                        templateUrl: 'templates/search.html',
                        controller: 'searchCtrl'
                    })
                    .state('notifications', {
                        url: '/notifications',
                        templateUrl: 'templates/notifications.html',
                        controller: 'notificationsCtrl',
                        cache:false
                    })
                    .state('messages', {
                        url: '/messages',
                        templateUrl: 'templates/messages.html',
                        controller: 'messagesCtrl',
                        cache:false
                    })
                    .state('projects', {
                        url: '/projects',
                        templateUrl: 'templates/projects.html',
                        controller: 'projectsCtrl',
                        cache:false
                    })
                    .state('project_history', {
                        url: '/project_history',
                        templateUrl: 'templates/project_history.html',
                        controller: 'projectHistoryCtrl',
                        cache:false
                    })
                    .state('message', {
                        url: '/message/:project_id',
                        templateUrl: 'templates/message.html',
                        controller: 'messageCtrl',
                        cache:false
                    })
                    ;

            // If user is logged in then redirect on user record page        
            if ($localStorageProvider.$get().user == undefined) {
                $urlRouterProvider.otherwise("/main");
            } else {
                $urlRouterProvider.otherwise("/home/");
            }
        });
