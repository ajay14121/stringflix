//
//  SoundVolumeManager.h
//  AuraApp
//
//  Created by Vitaliy T on 05/11/14.
//  Copyright (c) 2014 Quantum Life LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Notification name for receiving scanVolumeChange notifications
//NSString *kSoundVolumeManagerNotification_scanVolumeChanged = @"scanVolumeChanged";

@interface SoundVolumeManager : NSObject

//! Normalized value from "Scan Volume" slider with range [0.0; 1.0]
@property( nonatomic ) float scanVolumeSliderNormalizedValue;

//! Scan volume gain based of Logarithmic scale for "Scan Volume"
- (float) scanGain;

/** Human-readable value for gain in dB units.
 @example "Muted", "0dB", "-15dB"
 */
- (NSString *) stringRepresentationForLogScale;

+ (instancetype) sharedInstance;

@end
