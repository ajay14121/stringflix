//
//  oalQueuePlayback.h
//  AuraApp
//
//  Created by Svyatoshenko "Megal" Misha on 25.10.13.
//  Copyright (c) 2013 Quantum Life LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface oalQueuePlayback : NSObject <AVAudioSessionDelegate>

- (BOOL) isNewBufferNeeded;
- (void) addNewBuffer:(NSURL *)urlToWavFile;

/** Adds a new buffer
 *	@param rawData a pointer to a raw (lPCM) buffer data
 *	@param length length of rawBuffer in Bytes.
 *	@param channels 1 for MONO 2 for stereo. Stereo is recommended.
 *	@param bitResolution bits per one channel sample. 8/16. 16 is recommended
 *	@param samplingRate	samples per second. 44100 is common for audio.
 */
- (void) addNewBuffer:(void *)rawData
		   withLength:(size_t)length
		 withChannels:(int)channels
   withBitResoliution:(int)bitResolution
	 withSamplingRate:(int)samplingRate;

- (void) stopPlaying;
- (BOOL) startPlaying;

- (void) logSourceInfo;

- (void)setupVolume;
- (BOOL) setupSource;

//! remove used buffers from queue and set as available
- (void) cleanupProcessedBuffers;

+ (instancetype) sharedInstance;

@property( nonatomic, assign, readonly ) BOOL					isPaused;

@end
