#ifndef FastSinH
#define FastSinH
//------------------------------------------------------------------------------
// TFastSin
// calc y(n) = sin(n*W+B)
// W, B in RAD
// usage:
//     TFastSin fs(amp, Hz, samp, phase);
//     fs.calc();
//
class TFastSin {

private:

    double y0, y1, y2, p;
    double Zfade, xfade;

    double cW, cB, cA, x;
    double n;


public:
    bool isInit;

            TFastSin(void);
            TFastSin(double a, double w, double b);
            TFastSin(double amp, double Hz, double Samp, double phase);            
    void    init(double a, double w, double b);
    void    init(double amp, double Hz, double Samp, double phase);
    void    reset(void);
    double  calc(void);
    double  calcSin(double x);
    void    SetAmp(double amp);
    double  Freq2Inc(double freq, double samp);
    void    FillBuffer(short int*sbuff, int sb);
    double  initFade(double samp, double sec);
    double  Fade(void);

};
#endif
