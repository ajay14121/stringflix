//---------------------------------------------------------------------------
#ifndef WaveGenH
#define WaveGenH
//---------------------------------------------------------------------------

#include <stdio.h>
#include <math.h>
#include <string.h>
#include "FastSin.hpp"
#include "MusicFreq.h"

#define NiceDel(x) if (x!=NULL) { delete[]x; x=NULL;}
#define NiceDelNew(x,n) {if (x!=NULL) delete[]x; x=new TFastSin[n];}

typedef enum {CHAN_LEFT=1, CHAN_RIGHT=2} _type_channel;

typedef struct { // define a wave
    double amp;
    double Hz;
    double phase;
} TWaveDef;

class TWave : TMusicFreq  {
private:
    int chan;
    double Rphase, Samp;
    TFastSin *fsL, *fsR, *fsA;
    double *amp, *Hz, *phase, _amp, pulse, *Sinc, *Ainc;
    double t, inc;
    // bowl support
    TFastSin *fsBase, fsOsc, *fsDim, fsVibrato;
    
    void _init(void);
    
public:
    int n; // number of sine waves
    double cycle; // duration until re-start in secs
    double Ccycle;
    double fade_factor; // -0.2 is suitable for a 5 sec duration
    
    TWave();
    TWave(int pchan, double pSamp);
    ~TWave();
    void SetDif(double prPH);
    void SetAmp(double *Pamp, int chan);
    void SetAmp(double *Pamp);
    void SetL(double *Pamp, double *PHz, double *Pphase, int nv);
    void SetR(double *Pamp, double *PHz, double *Pphase, int nv);
    void Set(double *Pamp, double *PHz, double *Pphase, int nv);
    void Set(double*Pamp, double*PHz, double Ppulse);
    void gen(short int*sbuff, int sb);
    void genAM(short int*sbuff, int sb);
    void genAMFM(short int*sbuff, int sb);
    void genFaded(short int*sbuff, int sb);
    void AM_Modulate(int oct=-8);
    void Pulse(double hz);
    
    void Bowl(double Pamp, double w, double dim, double osc, int nh, bool vibrato);
    void genBowl(short int*sbuff, int sb);
    void genSingingBell(short int*sbuff, int sb);
    void ResetTime(void);
    void ResetTime(double elap);
};

#endif
