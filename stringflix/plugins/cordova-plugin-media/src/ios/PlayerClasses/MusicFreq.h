//---------------------------------------------------------------------------
#ifndef MusicFreqH
#define MusicFreqH

#include "ColorScale.h"


#define MUSICAL_INC 		      1.0594630943593 // 2^(1/12)
#define LOG_MUSICAL_INC     0.0577622650466
#define baseC0 			          261.62556530061  // 440 * MUSICAL_INC^(-9)
#define LOG_baseC0 		       5.5669143414923
#define LOG2 				           0.6931471805599

class TMusicFreq {
public:
  COLORREF Freq2Color(double freq);
  double FreqInOctave(double f, int oct);
  const char* NoteString(int i);
  const char* NoteString(double freq);
  double NoteOct2Freq(int note, int oct);
  int 	Freq2NoteOct( double freq, int&note, int&oct, char*&NoteOct, double&err );
  int 	Freq2NoteOct( double freq, int&note, int&oct, char*&NoteOct);
  const char* Freq2NoteOct( double freq );
  int 	Freq2Oct( double freq );
  int 	Freq2Note( double freq );
  double ErrInNote(double freq);
  bool 	FreqInOctRange(double freq, int octDown, int octUp);
  double Freq2Element(double freq);
  const char* Freq2StrNote(double freq);
  void   Frm2OCtaveRange(double *HzFrm,  int nform, int oct=0);
  double NoteFit(double hz);  
};
//---------------------------------------------------------------------------
#endif
