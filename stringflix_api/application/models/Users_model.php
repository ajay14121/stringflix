<?php

class Users_Model extends MY_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table = 'users';
        $this->validate = array(
            array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required',
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required',
            ),
        );
    }
     /**
     * @Name : check_login()
     * @Purpose : To validate the username and password.
     * @Call from : Authentication.php controller file.
     * @Functionality : Convert the value hash using sha1 algorithm functions.
     * @Receiver params : $value_to_hash value to be converted.
     * @Return params : return converted hash value.
     */

    function upload_video($data = array()){
      if ($this->db->insert('user_videos', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }  
    }
    function update_project_users($data = array(), $where = array()) {
        if (isset($data['password'])) {
            $data['password'] = $this->hash_this($data['password']);
        }
        $query = $this->db->update("project_users", $data, $where);
        return $query;
    }
    function logout($data = array()){
       if ($this->db->delete('auth_access_tokens', $data))
            return true;
        else
            return false;
         }
       function getUserDevices($users_id = '') {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join('auth_access_tokens at', $this->table . '.id=at.user_id and at.active="1"');
        $this->db->where($this->table . '.id IN (' . $users_id . ')');
        $this->db->where('at.active', '1');
        $query = $this->db->get();
        if ($query->num_rows()) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }
    
   
 
}
